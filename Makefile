PKGNAME=mig-admin-utils
PKGVERSION=$(shell cat VERSION)
PKGTAG=v${PKGVERSION}
FULLPKGNAME=${PKGNAME}-${PKGVERSION}
DISTDIR=dist/${FULLPKGNAME}
TARBALL=${FULLPKGNAME}.tgz
FILES=CHANGES LICENSE README VERSION Makefile ${PKGNAME}.spec
SCRIPTS=amqmon amqrm aplomgmt procstat s3t tcptop wypiwyc xsv

INSTROOT=
INSTLIBDIR=${INSTROOT}/usr/share/mig-admin-utils/lib
INSTBINDIR=${INSTROOT}/usr/bin
INSTMANDIR=${INSTROOT}/usr/share/man

.PHONY: build install tag sources rpm srpm clean

build:
	[ -d man ] || mkdir man
	@for name in ${SCRIPTS}; do \
	  pod2man --section=1 bin/$$name > man/$$name.1; \
	done
	ant -f java/build.xml

install:
	@for name in ${SCRIPTS}; do \
	  install -D -m 755 bin/$$name   ${INSTBINDIR}/$$name; \
	  install -D -m 644 man/$$name.1 ${INSTMANDIR}/man1/$$name.1; \
	done
	install -D -m 644 java/build/jar/mig-admin-utils.jar ${INSTLIBDIR}/mig-admin-utils.jar
	install -D -m 644 java/lib/jewelcli-0.8.9.jar ${INSTLIBDIR}/jewelcli-0.8.9.jar
	mv java/doc javadoc

tag:
	@seen=`git tag -l | grep -Fx ${PKGTAG}`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag ${PKGTAG}; \
	    git push --tags; \
	else \
	    echo "already tagged with ${PKGTAG}"; \
	fi

${TARBALL}: ${FILES}
	rm -rf dist
	mkdir -p ${DISTDIR}
	cp -a ${FILES} bin java ${DISTDIR}
	cd dist; tar cvfz ../${TARBALL} ${FULLPKGNAME}

sources: ${TARBALL}

rpm: ${TARBALL}
	rpmbuild -ta ${TARBALL}

srpm: ${TARBALL}
	rpmbuild -ts ${TARBALL}

clean:
	ant -f java/build.xml clean
	rm -rf man dist ${TARBALL}
