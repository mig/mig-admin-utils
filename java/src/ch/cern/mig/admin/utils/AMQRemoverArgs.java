/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils;

import java.util.List;

import com.lexicalscope.jewel.cli.CommandLineInterface;
import com.lexicalscope.jewel.cli.Option;
import com.lexicalscope.jewel.cli.Unparsed;

@CommandLineInterface(application="AMQRemover")
public interface AMQRemoverArgs {

    @Option(shortName="b", longName="broker", defaultValue="",
            description="broker name")
            String getBroker();

    @Option(shortName="C", longName="condition", defaultValue="",
            description="condition used to filter the objects")
            String getCondition();

    @Option(shortName="c", longName="conf", defaultValue="",
            description="configuration file to use")
            String getConfFile();

    @Option(shortName="f", longName="force",
            description="forced mode")
            boolean isForce();

    @Option(shortName="S", longName="globalsort",
            description="sort globally instead of grouping first by entity")
            boolean isSortGlobal();

    @Option(shortName="h", longName = "help", helpRequest = false,
            description = "show some help")
            boolean getHelp();

    @Option(shortName="H", longName="host", defaultValue="",
            description="broker host name")
            String getHost();

    @Option(shortName="i", longName="interactive",
            description="interactive mode")
            boolean isInteractive();

    @Option(shortName="l", longName="list",
            description="list the names defined in the configuration file")
            boolean isList();

    @Option(shortName="N", longName="name", defaultValue="",
            description="broker identifier in the configuration file")
            String getName();

    @Option(shortName="n", longName="noaction",
            description="noaction/dry mode")
            boolean isNoaction();

    @Option(shortName="o", longName="output", defaultValue="",
            description="output format to use")
            String getOutput();

    @Option(shortName="p", longName="port", defaultValue="1099",
            description="JMX port to connect to")
            int getPort();

    @Option(shortName="r", longName="regexp",
            description="treat the given names as regular expressions")
            boolean isRegExp();

    @Option(shortName="T", longName="timeout", defaultValue="1000000",
            description="timeout for each removal method invocation in milliseconds")
            long getTimeout();

    @Option(shortName="t", longName="type", defaultValue="destination",
            description="type of object being worked on")
            String getType();

    @Option(shortName="u", longName="user", defaultValue="",
            description="JMX user name")
            String getUser();

    @Unparsed(name="subjects")
    List<String> getSubjects();

}
