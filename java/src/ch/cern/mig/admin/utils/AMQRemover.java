/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.management.MalformedObjectNameException;

import ch.cern.mig.admin.utils.amqremover.AMQGenericRemover;
import ch.cern.mig.admin.utils.amqremover.AMQMultiRemover;
import ch.cern.mig.common.utils.PasswordField;

import com.lexicalscope.jewel.cli.ArgumentValidationException;
import com.lexicalscope.jewel.cli.CliFactory;

/**
 * <p>
 * Remove ActiveMQ destinations via JMX.<br><br>
 * Please check {@link #usage()} how to start the program.
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class AMQRemover {

    //
    // private members for internal use (connection/username/passwd...)
    //
    private static int mode = AMQGenericRemover.INTERACTIVE_MODE;
    private static final String[] types = {"destination", "connection", "peer"};
    private static Properties conf = null;
    private static final String defaultConfigFile = System.getProperty("user.home") + File.separatorChar + ".amqrm.conf";
    private static String configFile = null;
    private static String[] knownNames = null;
    private static AMQMultiRemover amqmr;

    /** Prints the signature of this program.<br><br>
     * <code>
     * java ch.cern.mig.admin.utils.AMQRemover [options] names...
     * </code>
     *
     */
    public static void usage() {
        System.out.println("Usage: amqrm [options] names... " +
                           "(run 'amqrm -h' for the list of options)");
    }

    /**
     * Load config file
     */
    private static void loadConfigFile() {
        conf = new Properties();
        try {
            conf.load(new FileInputStream(configFile));
        } catch (Exception e) {
            System.err.println("Error: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }

    /** Reads the credentials from a file
     *
     * @param filename The filename with the credentials
     * @see {@link #main(String[])}
     */
    private static Hashtable<String, Object> readCredentialsFromFile(String name) {
        Hashtable<String, Object> credentials = new Hashtable<String, Object>();
        credentials.put("jmx.remote.credentials",
                        new String[] {
                            conf.getProperty(name + ".user", conf.getProperty("default.user", "")),
                            conf.getProperty(name + ".password", conf.getProperty("default.password", "")) });
        return credentials;
    }

    /** Reads the credentials from command line
     *
     * @param user The username
     * @see {@link #main(String[])}
     */
    private static Hashtable<String, Object> readCredentialsInteractive(String host, String user) {
        Hashtable<String, Object> credentials = new Hashtable<String, Object>();
        String password = null;

        try {
            if (user.length() == 0) {
                System.out.print("Enter user for " + host + ": ");
                user = String.valueOf(new BufferedReader(new InputStreamReader(System.in)).readLine());
            }
            password = String.valueOf(PasswordField.getPassword(System.in, "Enter password for " + user + "@" + host + ": "));
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            System.exit(1);
        }

        credentials.put("jmx.remote.credentials", new String[] { user, password == null ? "" : password });
        return credentials;
    }

    /** Reads program arguments.<br />
     *
     * @param args Program arguments (@see {@link #usage()}
     */
    private static AMQRemoverArgs parseArguments(final String[] args) {
        AMQRemoverArgs arguments = null;
        try {
            arguments = CliFactory.parseArguments(AMQRemoverArgs.class, args);
            if (!arguments.isList()) {
                if (arguments.getSubjects() == null) {
                    throw new Exception("You have to specify at least one name.");
                }
                if (arguments.getHost().trim().length() == 0 && arguments.getName().trim().length() == 0) {
                    throw new Exception("You have to specify at least one host or one entity name for the configuration file.");
                }
                if (Arrays.asList(types).indexOf(arguments.getType()) < 0) {
                    throw new Exception("No valid type: " + arguments.getType());
                }
            }
        } catch (ArgumentValidationException e) {
            usage();
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            usage();
            System.err.println(e.getMessage());
            System.exit(1);
        }
        if (arguments.getSubjects() != null) {
            if (! arguments.getType().equals("destination")) lowerAll(arguments.getSubjects());
            if (! arguments.isRegExp()) quoteAll(arguments.getSubjects());
        }
        return arguments;
    }

    public static void quoteAll(List<String> list) {
        ListIterator<String> li = list.listIterator();
        while (li.hasNext()) {
            String val = li.next();
            li.remove();
            li.add(Pattern.quote(val));
        }
    }

    public static void lowerAll(List<String> list) {
        for (int c = 0; c < list.size(); c++) list.set(c, list.get(c).toLowerCase());
    }

    private static void addHost(String host, int port, String brokername,
                                Hashtable<String, Object> jmxOptions) {
        addHost(host, host, port, brokername, jmxOptions);
    }

    private static void addHost(String entity, String host, int port, String brokername,
                                Hashtable<String, Object> jmxOptions) {

        try {
            amqmr.addHost(entity, host, port, brokername, jmxOptions);
        } catch (IOException e) {
            System.err.println("Error with " + entity + ", skipping it: " + e.getMessage());
        } catch (SecurityException e) {
            System.err.println("Invalid username and password for " + entity + ", skipping it: " + e.getMessage());
        } catch (MalformedObjectNameException e) {
            System.err.println("Error with " + entity + ", skipping it: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Error with " + entity + ", skipping it: " + e.getMessage());
        }
    }

    private static void addCmdHosts(AMQRemoverArgs arguments) {

        String[] hosts = arguments.getHost().split(",");
        for (String host : hosts) {
            Hashtable<String, Object> jmxOptions = readCredentialsInteractive(host, arguments.getUser());
            String brokername = arguments.getBroker().length() > 0 ? arguments.getBroker() : null;
            addHost(host, arguments.getPort(), brokername, jmxOptions);
        }
    }

    private static void addNamedHosts(AMQRemoverArgs arguments) {
        if (conf == null) {
            System.err.println("Missing configuration file for this operation.");
            System.exit(1);
        }
        String[] names = null;
        if (arguments.getName().equals("ALL")) {
            names = knownNames;
        } else {
            HashSet<String> hs = new HashSet<String>();
            for (String name : arguments.getName().split(",")) {
                if (name.matches("[\\w\\-\\@]+")) {
                    if (!hs.contains(name)) hs.add(name);
                } else {
                    for (String known : knownNames) {
                        if (known.matches(name) && !hs.contains(known)) hs.add(known);
                    }
                }
            }
            names = (String[])hs.toArray(new String[hs.size()]);
            Arrays.sort(names);
        }

        for (String name : names) {
            String host = conf.getProperty(name + ".host", null);
            if (host == null) {
                System.err.println("Host missing for " + name + ".");
                continue;
            }
            String brokername =  conf.getProperty(name + ".broker");
            int port = Integer.parseInt(conf.getProperty(name + ".port",
                conf.getProperty("default.port", "" + arguments.getPort())));
            Hashtable<String, Object> jmxOptions = readCredentialsFromFile(name);
            addHost(name, host, port, brokername, jmxOptions);
        }
    }

    private static void setConfig(AMQRemoverArgs arguments) {
        configFile = arguments.getConfFile();
        if (configFile.length() == 0) {
            File f = new File(defaultConfigFile);
            if (f.exists()) configFile = defaultConfigFile;
        }
    }

    private static void findKnown(AMQRemoverArgs arguments) {
        HashSet<String> hs = new HashSet<String>();
        List<Object> keys = Collections.list((Enumeration<Object>) conf.keys());
        for (Object key : keys) {
            String name = key.toString().split("\\.", 2)[0];
            if (!(name.equals("default") || hs.contains(name))) hs.add(name);
        }
        knownNames = (String[])hs.toArray(new String[hs.size()]);
        Arrays.sort(knownNames);
        if (arguments.isList()) {
            for (String name : knownNames) {
                System.out.println(name);
            }
            System.exit(0);
        }
    }

    private static void setMode(AMQRemoverArgs arguments) {
        mode = arguments.isForce() ? AMQGenericRemover.FORCE_MODE : mode;
        mode = arguments.isNoaction() ? AMQGenericRemover.NOACTION_MODE : mode;
        mode = arguments.isInteractive() ? AMQGenericRemover.INTERACTIVE_MODE : mode;
    }

    /** Reads program arguments and sets -if given- the credentials.<br>
     *
     * @param args Program arguments (@see {@link #usage()}
     */
    public static void main(final String[] args) {
        AMQRemoverArgs arguments = parseArguments(args);
        setConfig(arguments);
        if (configFile.length() > 0) loadConfigFile();
        if (conf == null) {
            if (arguments.isList()) {
                usage();
                System.err.println("Option --list requires a configuration file.");
                System.exit(1);
            }
        } else {
            findKnown(arguments);
        }
        setMode(arguments);

        try {
            amqmr = new AMQMultiRemover(mode, arguments.getType(), arguments.getOutput(), arguments.getTimeout());
        } catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }

        if (arguments.getHost().trim().length() > 0) addCmdHosts(arguments);
        if (arguments.getName().trim().length() > 0) addNamedHosts(arguments);

        boolean ret = amqmr.removeFromAll(arguments.getSubjects(), arguments.getCondition(), arguments.isSortGlobal());
        amqmr.removeAllHosts();
        System.exit(ret ? 0 : 1);
    }

}
