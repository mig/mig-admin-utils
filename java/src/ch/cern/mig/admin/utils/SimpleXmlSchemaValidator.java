/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.apache.xerces.jaxp.validation.XMLSchemaFactory;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.ArgumentValidationException;
import com.lexicalscope.jewel.cli.CliFactory;

/** 
 * <p>
 * Validate XML files against multiple XML schemas.<br /><br/ > 
 * Please check {@link #usage()} how to start the program
 * </p>
 * 
 */
public class SimpleXmlSchemaValidator {
	
	private static Logger log = Logger.getLogger(SimpleXmlSchemaValidator.class.getName());
	private List<String> files;
	private Schema schema;

	/** Constructor.
	 * 
	 * @param files The list of XML files to validate
	 * @param schemas The list of XSD schemas to use for the validation
	 * @throws SAXException 
	 */
	public SimpleXmlSchemaValidator (List<String> files, List<String> schemas) throws SAXException {
		this.files = files;
		
		log.info("Getting schemas");
		List<StreamSource> ss = new ArrayList<StreamSource>();
		for (Iterator<String> i = schemas.iterator(); i.hasNext(); ) {
			String name = i.next();
			log.info("Adding schema " + name);
			ss.add(new StreamSource(new File(name)));
		}
		
		XMLSchemaFactory xmlSchemaFactory = new XMLSchemaFactory();
		schema = xmlSchemaFactory.newSchema(ss.toArray(new StreamSource[0]));
	}
	
	/** Validate.<br />
	 * 
	 */
	public void validate() {
		Validator validator = schema.newValidator();
		for (Iterator<String> i = files.iterator(); i.hasNext(); ){
			System.out.println("");
			String file = i.next();
			log.info("Validating file " + file);
			try{
				validator.validate(new StreamSource(new File(file)));
			} catch (Exception e) {
				System.err.println("- Error validating " + file + "\n" + e);
				System.exit(1);
			}
			System.out.println("- File " + file + " is valid.");
		}
	}
	
	/** Prints the signature of this program.<br><br>
	 * <code>
	 * java ch.cern.mig.admin.utils.SimpleXmlSchemaValidator -s|--schema file-or-directory [-v] file1 file2 file3 ...
	 * </code>
	 * 
	 */
	public static void usage() {
		System.out.println("" +
				"Usage: simple-xml-schema-validator options files-to-validate..." +
				"\n\t--schema -s <file-or-directory> : xml schema file or directory containing schemas" +
				"\n\t--verbose -v : verbose" +
				"\n\t[--help] : display help" );
	}
	
	/** Reads program arguments.<br />
	 * 
	 * @param args Program arguments (@see {@link #usage()}
	 */
	private static SimpleXmlSchemaValidatorArgs parseArguments(final String[] args) {
		SimpleXmlSchemaValidatorArgs arguments = null;
		
		try {
			arguments = CliFactory.parseArguments(SimpleXmlSchemaValidatorArgs.class, args);
			if (arguments.getFiles() == null) {
				throw new ArgumentValidationException("You have to specify at least one file to validate");
			}
		} catch(ArgumentValidationException e) {
			usage();
			System.out.println(e.getMessage());
			System.exit(1);
		}
		return arguments;
	}
	
	public static class XsdFilenameFilter implements FilenameFilter {
		
		public boolean accept(File dir, String name) {
			if (name.endsWith("xsd")) {
				return true;
			}
			return false;
		}
	}
	
	/** Check XSD schemas passed by parameters.<br />
	 * 
	 * @param schemaPath Schema path (@see {@link #usage()}
	 */
	private static List<String> getSchemas(String schemaPath) {
		List<String> schemas = new ArrayList<String>();
		
		log.info("Getting the list of files");
		File f = new File(schemaPath);
		if ((! f.exists()) || (! f.canRead())){
			return schemas;
		}
		if(f.isFile()){
			schemas.add(f.getAbsolutePath());
			log.info("Schema " + f.getPath() + "added");
		} else if (f.isDirectory()){
			File[] ff = f.listFiles(new XsdFilenameFilter());
			if (ff != null) {
			    for (int i=0; i<ff.length; i++) {
			        schemas.add(ff[i].getPath());
			        log.info("Schema " + ff[i].getPath() + "added");
			    }
			}
		}
		
		return schemas;
	}
	
	/** Check files passed for validation.<br />
	 * 
	 * @param files Program arguments (@see {@link #usage()}
	 */
	private static List<String> checkFiles(List<File> files) {
		List<String> newFiles = new ArrayList<String>();
		for (Iterator<File> i = files.iterator(); i.hasNext(); ){
			File f = i.next();
			if (f.exists() && f.canRead()) {
				newFiles.add(f.getPath());
				log.info("File " + f.getPath() + " added");
			}
		}
		return newFiles;
	}
	
	/** Reads program arguments and launch validation.<br>
	 * 
	 * @param args Program arguments (@see {@link #usage()}
	 */
	public static void main(final String[] args) {
		
		SimpleXmlSchemaValidatorArgs arguments = parseArguments(args);
		if(arguments.isVerbose()){
			log.setLevel(Level.INFO);
		}else{
			log.setLevel(Level.WARNING);
		}
		List<String> schemas = getSchemas(arguments.getSchema());
		List<String> files = checkFiles(arguments.getFiles());
		if(files.size()==0){
			System.err.println("No valid XML files specified.");
			System.exit(1);
		}
		
		log.info("> Initializing schema validator");
		SimpleXmlSchemaValidator l = null;
		try {
			l = new SimpleXmlSchemaValidator(files, schemas);
		} catch (SAXException e) {
			log.warning("Error during schemas loading: " + e.getMessage());
			System.exit(1);
		}
		log.info("Starting validation");
		l.validate();
		
		System.exit(0);
	}

}
