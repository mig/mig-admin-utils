/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.util.HashMap;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 *
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class EnrichedObjectName extends ObjectName {

    private HashMap<String,Object> extra;

    public EnrichedObjectName(String name) throws MalformedObjectNameException, NullPointerException {
        super(name);
        extra = new HashMap<String, Object>();
    }

    public void putExtra(String key, Object obj){
        extra.put(key, obj);
    }

    public boolean hasExtra(String key){
        return extra.containsKey(key);
    }

    public Object getExtra(String key){
        return extra.get(key);
    }

}
