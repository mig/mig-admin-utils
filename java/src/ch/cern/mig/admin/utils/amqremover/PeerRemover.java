/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.script.ScriptException;

import ch.cern.mig.common.utils.ExpEval;
import ch.cern.mig.common.utils.NetworkUtils;

/**
 * <p>
 * Remove ActiveMQ connections via JMX per address.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class PeerRemover extends AMQGenericRemover {

    public List<String> supported_columns_o = Arrays.asList("entity", "host", "connections", "address", "hostname");
    public List<String> default_columns_o = Arrays.asList("entity", "connections", "address", "hostname");
    private static final Map<String, String> headers_title_o = new HashMap<String, String>();

    static {
        Map<String, String> map = headers_title_o;
        map.put("id", "ID");
        map.put("entity", "ENTITY");
        map.put("host", "HOST");
        map.put("connections", "CONNECTIONS");
        map.put("address", "ADDRESS");
        map.put("hostname", "HOSTNAME");
    }

    /** Constructor.
     *
     * @param mode The removal mode
     */
    public PeerRemover (int mode) {
        super(mode);
        supported_columns = supported_columns_o;
        default_columns = default_columns_o;
        columns = new ArrayList<String>(default_columns);
        headers_title.putAll(headers_title_o);
    }

    public class SortByCount implements Comparator<EnrichedObjectName>{

        public int compare(EnrichedObjectName o1, EnrichedObjectName o2) {
            @SuppressWarnings("unchecked")
                final int size1 = ((List<ObjectName>) o1.getExtra("objectNames")).size();
            @SuppressWarnings("unchecked")
                final int size2 = ((List<ObjectName>) o2.getExtra("objectNames")).size();
            if (size1 < size2) return -1;
            if (size1 > size2) return 1;
            return 0;
        }

    }

    /**
     * @return Returns a list of all Connections to remove from AMQ JMX domain
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     * @throws ScriptException
     */
    public List<EnrichedObjectName> getObjects(AMQBrokerConnection amqbc, List<String> filter, String condition)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, ScriptException {
        String objectName = amqbc.isOldType() ? "org.apache.activemq:BrokerName=" + amqbc.brokername + ",Type=Connection,Connection=*,*" :
            "org.apache.activemq:type=Broker,brokerName=" + amqbc.brokername + ",connector=clientConnectors,connectorName=*,connectionName=*,*";
        List<ObjectName> ons = new ArrayList<ObjectName>(amqbc.mbsc.queryNames(
                                                                               new ObjectName(objectName), null));
        List<EnrichedObjectName> ips = new ArrayList<EnrichedObjectName>();
        Pattern p = Pattern.compile("//(.*):.*");
        Hashtable<String, List<ObjectName>> elms = new Hashtable<String, List<ObjectName>>();
        ListIterator<ObjectName> it = ons.listIterator();
        while (it.hasNext()) {
            ObjectName on = it.next();
            String remoteAddress = "";
            try {
                remoteAddress = amqbc.mbsc.getAttribute(on, "RemoteAddress").toString();
            } catch (InstanceNotFoundException e) {
                continue;
            }
            Matcher m = p.matcher(remoteAddress.toString());
            if (! m.find()) continue;
            if (! (objectNameMatchesOne(m.group(1), filter) || objectNameMatchesOne(NetworkUtils.getHostname(m.group(1)), filter))) continue;
            List<ObjectName> l = elms.get(m.group(1));
            if (elms.get(m.group(1)) == null) l = new ArrayList<ObjectName>();
            l.add(on);
            elms.put(m.group(1), l);
        }
        Enumeration<String> e = elms.keys();
        while (e.hasMoreElements()) {
            String cur = e.nextElement();
            Map <String, String> params = new HashMap<String, String>();
            params.put("connections", "" + elms.get(cur).size());
            if (! ExpEval.eval(condition, params)) continue;
            EnrichedObjectName eon = new EnrichedObjectName("connection:address=" + cur + ",connectionCount=" + elms.get(cur).size());
            eon.putExtra("objectNames", elms.get(cur));
            if (columns.contains("address")) eon.putExtra("address", cur);
            if (columns.contains("hostname")) eon.putExtra("hostname", NetworkUtils.getHostname(cur));
            if (columns.contains("connections")) eon.putExtra("connections", elms.get(cur).size());
            ips.add(eon);
        }
        sortObjects(ips);
        return new ArrayList<EnrichedObjectName>(ips);
    }

    /**
     * @param objects
     */
    public void sortObjects(List<EnrichedObjectName> objects){
        Collections.sort(objects, new SortByCount());
    }

    private boolean objectNameMatchesOne(String ip, List<String> filter) {
        for(String el: filter){
            if (ip.toString().matches(el)) return true;
        }
        return false;
    }

    /** Remove an object if it needs to be removed.
     *
     * @param amqbc The broker connection
     * @param object An ObjectName
     * @throws RemoverException
     */
    public void removeObject(AMQBrokerConnection amqbc, EnrichedObjectName object, long timeout) throws RemoverException {
        if (object == null) return;
        EnrichedObjectName eon = object;
        @SuppressWarnings("unchecked")
            List<ObjectName> elms = (List<ObjectName>) eon.getExtra("objectNames");
        if(elms == null) {
            throw new RemoverException("Error removing " + object);
        }
        ListIterator<ObjectName> li = elms.listIterator();
        int deleted = 0;
        String errorMessage = "";
        while (li.hasNext()) {
            ObjectName cur = li.next();
            try {
                amqbc.invoke(
                             cur,
                             "stop", null , null, timeout);
                deleted++;
            } catch(TimeoutException e){
                errorMessage += "Timeout removing: " + cur + "\n";
            } catch (Exception e) {
                errorMessage += "Error removing " + cur + ": " + e + "\n";
            }
        }
        if(errorMessage.length() > 0)
            throw new RemoverException(errorMessage);
    }

}
