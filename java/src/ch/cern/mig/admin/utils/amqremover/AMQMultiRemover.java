/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.management.MalformedObjectNameException;
import javax.script.ScriptException;

/**
 * <p>
 * ActiveMQ multi-host remover via JMX.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class AMQMultiRemover {

    protected String type;
    protected int mode = AMQGenericRemover.INTERACTIVE_MODE;
    protected long timeout;
    private Map<String, AMQBrokerConnection> hosts = new HashMap<String, AMQBrokerConnection>();
    protected AMQGenericRemover amqgr;

    /** Constructor.
     *
     * @param mode The removal mode
     * @throws Exception
     */
    public AMQMultiRemover(int mode, String type, String output, long timeout) throws Exception {
        this.mode = mode;
        this.type = type;
        this.timeout = timeout;
        if(type.equals("connection")) {
            amqgr = new ConnectionRemover(mode);
        }else if(type.equals("peer")) {
            amqgr = new PeerRemover(mode);
        }else if(type.equals("destination")) {
            amqgr = new DestinationRemover(mode);
        }else{
            throw new Exception("Type " + type + " not recognized");
        }
        amqgr.setOutputFormat(output);
    }

    /** Add a host to the list
     *
     * @param hostname The hostname to connect to
     * @param brokername The brokername to connect to (stated in activemq.xml)
     * @param port The <b>JMX</b> port of the broker
     * @param credentials The credentials for the connection
     * @throws Exception
     * @throws MalformedObjectNameException
     */
    public void addHost(String hostname, int port, String brokername, Hashtable<String,Object> credentials) throws MalformedObjectNameException, Exception {
        addHost(hostname, hostname, port, brokername, credentials);
    }

    /** Establishes the connection to the JMX management interface.
     *
     * @param entity Readable name for the host
     * @param hostname The hostname to connect to
     * @param brokername The brokername to connect to (stated in activemq.xml)
     * @param port The <b>JMX</b> port of the broker
     * @param credentials The credentials for the connection
     * @throws Exception
     * @throws MalformedObjectNameException
     */
    public void addHost(String entity, String hostname, int port, String brokername, Hashtable<String,Object> credentials) throws MalformedObjectNameException, Exception {
        AMQBrokerConnection host = new AMQBrokerConnection(entity, hostname, port, brokername, credentials);
        host.connect();
        hosts.put(entity, host);
    }

    /** Connect to all the hosts.
     * @throws Exception
     * @throws SecurityException
     * @throws MalformedObjectNameException
     */
    public void connectAllHosts() throws MalformedObjectNameException, SecurityException, Exception{
        for(String name : hosts.keySet()) {
            connect(name);
        }
    }

    /** Connect to the specified host.
     *
     * @param name The name of the broker to connect to.
     * @throws Exception
     * @throws SecurityException
     * @throws MalformedObjectNameException
     */
    public void connect(String name) throws MalformedObjectNameException, SecurityException, Exception {
        if(hosts.get(name) != null) hosts.get(name).connect();
    }

    /** Closes all the JMX connections we keep.
     *
     */
    public void removeAllHosts() {
        for(Iterator<Map.Entry<String, AMQBrokerConnection>> it = hosts.entrySet().iterator(); it.hasNext();) {
            AMQBrokerConnection b = it.next().getValue();
            b.disconnect();
            it.remove();
        }
    }

    /** Closes the JMX connection we keep.
     *
     * @param name The name of the broker to remove.
     */
    public void removeHost(String name) {
        AMQBrokerConnection b = hosts.remove(name);
        b.disconnect();
    }

    /** Remove items from all the hosts specified depending on subjects and conditions.
     *
     * @param subjects The subjects to remove.
     * @param condition The condition to remove an item.
     */
    public boolean removeFromAll(List<String> subjects, String condition, boolean globalSort) {
        boolean status = true;
        List<EnrichedObjectName> objects = getObjectsFromAll(subjects, condition);
        if(objects.size() == 0) {
            System.out.println("No items found.");
            return status;
        }
        if(globalSort) amqgr.sortObjects(objects);

        StringTable st = getTable(objects);

        //print && remove
        if(mode == AMQGenericRemover.INTERACTIVE_MODE) {
            st.printTable();

            System.out.print("\nWhich items do you want to delete?\nEnter a comma or space seprated ID list, then press return: ");
            java.io.BufferedReader stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
            String line;
            try {
                line = stdin.readLine();
            } catch (IOException e1) {
                System.err.println("Error reading from command line.");
                line = "";
            }
            String[] todelete = line.trim().split("[ ,]+");
            for (String item : todelete) {
                int id;
                try{
                    id = Integer.parseInt(item);
                } catch (Exception e ) {
                    System.err.println("Error: " + item + " is not a valid ID value.");
                    continue;
                }
                if (id >= 0 && id < objects.size()) {
                    try {
                        amqgr.removeObject((AMQBrokerConnection) objects.get(id).getExtra("amqbc"), (EnrichedObjectName) objects.get(id), this.timeout);
                        System.out.println("Element " + id + " removed correctly.");
                    }catch(RemoverException e) {
                        System.err.println("Error removing element " + id + ": " + e);
                        status = false;
                    }
                }else{
                    System.err.println("Error: " + id + " is not a valid ID value.");
                }
            }
        }else{ // NOACTION or FORCE mode
            st.printlnRow(0); // printing headers
            int c = 1;
            for(EnrichedObjectName on : objects) {
                AMQBrokerConnection host = (AMQBrokerConnection) on.getExtra("amqbc");
                st.printRow(c++);
                if (mode == AMQGenericRemover.FORCE_MODE) {
                    System.out.print("removing... ");
                    try {
                        amqgr.removeObject(host, on, this.timeout);
                        System.out.print("DONE");
                    } catch (RemoverException e) {
                        System.out.print("FAILED " + e);
                        status = false;
                    }
                }
                System.out.println();
            }
        }
        return status;
    }

    /**
     * @param objects The objects that should appear on the table.
     * @return StringTable The table containing the result.
     */
    public StringTable getTable(List<EnrichedObjectName> objects) {
        if(mode == AMQGenericRemover.INTERACTIVE_MODE) amqgr.addColumn(0, "id");
        StringTable st = new StringTable(objects.size() + 1, amqgr.columns.size());
        st.addRow(amqgr.getHeadersValues());
        for(int i = 0; i < objects.size(); i++) {
            EnrichedObjectName on = objects.get(i);
            AMQBrokerConnection host = (AMQBrokerConnection) on.getExtra("amqbc");
            if(mode == AMQGenericRemover.INTERACTIVE_MODE) on.putExtra("id", i);
            st.addRow(amqgr.getItemValues(host, on));
        }
        return st;
    }

    /**
     * @param subjects The subjects to remove.
     * @param condition The conditions to remove the objects.
     * @return StringTable The table containing the result.
     */
    public List<EnrichedObjectName> getObjectsFromAll(List<String> subjects, String condition) {
        List<EnrichedObjectName> objects = new ArrayList<EnrichedObjectName>();
        for(Iterator<Map.Entry<String, AMQBrokerConnection>> it = hosts.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, AMQBrokerConnection> entry = it.next();
            try {
                List<EnrichedObjectName> temp = amqgr.getObjects(entry.getValue(), subjects, condition);
                for(EnrichedObjectName o : temp) {
                    o.putExtra("amqbc", entry.getValue());
                    objects.add(o);
                }
            } catch (ScriptException ex) {
                System.err.println("Error on condition evaluation: " + condition + "\nPlease review your condition expression.\n" + ex);
                System.exit(1);
            } catch (Exception ex) {
                System.err.println("Error fetching items from " + entry.getKey() + ": " + ex);
                ex.printStackTrace();
                it.remove();
            }
        }
        return objects;
    }
}
