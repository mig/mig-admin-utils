package ch.cern.mig.admin.utils.amqremover;

public class RemoverException extends Exception {

    String message;

    public RemoverException() {
        super();
    }

    public RemoverException(String message) {
        super(message);
        this.message = message;
    }

    public String toString(){
        return message;
    }

}
