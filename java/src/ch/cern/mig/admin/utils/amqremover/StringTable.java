package ch.cern.mig.admin.utils.amqremover;

public class StringTable{

    private int counter = 0;
    private String[][] data;
    private int[] lengths;

    public StringTable(int n, int m){
        data = new String[n][m];
        lengths = new int[m];
    }

    public void addRow(String[] row){
        setRow(counter++, row);
    }

    public void setRow(int r, String[] row){
        data[r] = row;
        for (int i = 0; i < data[r].length; i++) {
            if(data[r][i] != null)
                lengths[i] = Math.max(data[r][i].length(), lengths[i]);
        }
    }

    public void printlnRow(int r){
        printRow(r);
        System.out.println();
    }

    public void printRow(int r){
        for (int i = 0; i < data[r].length; i++) {
            System.out.print(String.format("%1$-" + lengths[i] + "s ", data[r][i]));
        }
    }

    public void printTable(){

        for (int i = 0; i < data.length; i++) {
            printlnRow(i);
        }
    }

}
