/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.script.ScriptException;

import ch.cern.mig.common.utils.ExpEval;

/**
 * <p>
 * Remove ActiveMQ destinations via JMX.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class DestinationRemover extends AMQGenericRemover {

    public static List<String> supported_columns_o = Arrays.asList("entity", "host", "size", "dequeued", "consumers", "destination");
    public static List<String> default_columns_o = Arrays.asList("entity", "size", "dequeued", "consumers", "destination");
    private static final Map<String, String> headers_title_o = new HashMap<String, String>();

    static {
        Map<String, String> map = headers_title_o;
        map.put("id", "ID");
        map.put("entity", "ENTITY");
        map.put("host", "HOST");
        map.put("size", "SIZE");
        map.put("dequeued", "DEQUEUED");
        map.put("consumers", "CONSUMERS");
        map.put("destination", "DESTINATION");
    }

    /** Constructor.
     *
     * @param mode The removal mode
     */
    public DestinationRemover (int mode) {
        super(mode);
        supported_columns = supported_columns_o;
        default_columns = default_columns_o;
        columns = new ArrayList<String>(default_columns);
        headers_title.putAll(headers_title_o);
    }

    /**
     * @return Returns a list of all Queues from AMQ JMX domain
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     */
    public ObjectName[] getQueues(AMQBrokerConnection amqbc)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, NullPointerException {
        return get(amqbc, "Queues");
    }

    /**
     * @return Returns a list of all Durable Topics Subscribers from AMQ JMX domain
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     */
    public ObjectName[] getDurableTopicSubscribers(AMQBrokerConnection amqbc)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, NullPointerException {
        return get(amqbc, "DurableTopicSubscribers");
    }

    /**
     * @return Returns a list of all Topics from AMQ JMX domain
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     */
    public ObjectName[] getTopics(AMQBrokerConnection amqbc)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, NullPointerException {
        return get(amqbc, "Topics");
    }

    /** Fetches the list of ObjectName(s) from a given AMQ JMX attribute.
     *
     * @see {@link #getTopics()}, {@link #getQueues()}
     * @param amqbc The broker connection
     * @param attribute name the attribute from <code>org.apache.activemq:BrokerName=" + brokername + ",Type=Broker</code> domain
     *
     * @return Array of ObjectName
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     */
    private ObjectName [] get(AMQBrokerConnection amqbc, String attribute)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, NullPointerException {
        String objectName = amqbc.isOldType() ? "org.apache.activemq:BrokerName=" + amqbc.brokername + ",Type=Broker" :
            "org.apache.activemq:type=Broker,brokerName=" + amqbc.brokername;
        return (ObjectName[]) amqbc.mbsc.getAttribute(
                                                      new ObjectName(objectName), attribute);
    }

    /**
     * @return a list of all destinations
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws IOException
     * @throws MalformedObjectNameException
     */
    private List<ObjectName> getAllObjects(AMQBrokerConnection amqbc) throws AttributeNotFoundException,
                                                                             InstanceNotFoundException, MBeanException, ReflectionException,
                                                                             IOException, MalformedObjectNameException {
        ObjectName[] q = (ObjectName[]) getQueues(amqbc);
        ObjectName[] t = (ObjectName[]) getTopics(amqbc);
        ObjectName[] d =  new ObjectName[q.length + t.length];
        System.arraycopy(q, 0, d, 0, q.length);
        System.arraycopy(t, 0, d, q.length, t.length);
        Arrays.sort(d);
        List<ObjectName> ons = new ArrayList<ObjectName>(Arrays.asList(d));

        return ons;
    }

    /**
     * @return Returns a list of all destinations to remove
     *
     * @param amqbc The broker connection
     * @param filter The list of destinations
     * @param condition The condition
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     * @throws ScriptException
     */
    public List<EnrichedObjectName> getObjects(AMQBrokerConnection amqbc, List<String> filter, String condition)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, ScriptException {
        List<EnrichedObjectName> result = new ArrayList<EnrichedObjectName>();
        List<ObjectName> ons = getAllObjects(amqbc);

        for (ObjectName on : ons) {
            AttributeList al = null;
            String name = "";
            long destinationSize = 0;
            long consumerCount = 0;
            long dequeueCount = 0;
            try{
                al = amqbc.mbsc.getAttributes(on, new String[] {"Name", "QueueSize", "ConsumerCount", "DequeueCount"});
                name = getValueFromAttribute(al, 0);
                destinationSize = Long.parseLong(getValueFromAttribute(al, 1));
                consumerCount = Long.parseLong(getValueFromAttribute(al, 2));
                dequeueCount = Long.parseLong(getValueFromAttribute(al, 3));
            }catch(InstanceNotFoundException e) {
                continue;
            }
            boolean isQueue = on.toString().contains("Type=Queue");
            if (! destinationMatchOne(name, isQueue, filter)) {
                continue;
            }
            Map <String, String> params = new HashMap<String, String>();
            params.put("consumers", "" + consumerCount);
            params.put("size", "" + destinationSize);
            params.put("dequeued", "" + dequeueCount);
            if (! ExpEval.eval(condition, params)) continue;

            EnrichedObjectName non = new EnrichedObjectName(on.toString());
            non.putExtra("name", name);
            if (columns.contains("destination")) non.putExtra("destination", (isQueue ? "/queue/" : "/topic/") + name);
            if (columns.contains("size")) non.putExtra("size", "" + destinationSize);
            if (columns.contains("dequeued")) non.putExtra("dequeued", "" + dequeueCount);
            if (columns.contains("consumers")) non.putExtra("consumers", "" + consumerCount);

            result.add(non);
        }
        return result;
    }

    /** Remove a destination if it needs to be removed.
     *
     * @param object An ObjectName
     * @throws Exception
     */
    public void removeObject(AMQBrokerConnection amqbc, EnrichedObjectName object, long timeout) throws RemoverException{
        boolean isQueue = object.toString().contains("Type=Queue");
        String name = object.getExtra("name").toString();
        String fullname = object.getExtra("destination").toString();

        String[] signature = {new String("java.lang.String")};
        Object[] params = {name};

        if (! isQueue) removeDurableTopicSubscribers(amqbc, name, timeout);
        String objectName = amqbc.isOldType() ? "org.apache.activemq:BrokerName=" + amqbc.brokername + ",Type=Broker" :
            "org.apache.activemq:type=Broker,brokerName=" + amqbc.brokername + "";
        try{
            amqbc.invoke(
                         new ObjectName(objectName), (isQueue ? "removeQueue" : "removeTopic"),
                         params , signature, timeout);
        } catch(TimeoutException e) {
            throw new RemoverException("Timeout removing: " + fullname);
        } catch (Exception e) {
            throw new RemoverException("Error removing " + fullname + ": " + e);
        }
    }

    /**
     * Remove Durable Topic Subscriber.
     *
     * @param destination The destination name for the durable topic subscribers to remove.
     */
    private void removeDurableTopicSubscribers(AMQBrokerConnection amqbc, String destination, long timeout) {

        ObjectName[] dts = null;
        try {
            dts = getDurableTopicSubscribers(amqbc);
        } catch (Exception ex) {
            System.err.println("Error getting durable topic subscribers" + ex);
        }
        if (dts == null) return;
        int ok = 0, failed = 0;
        for (int j = 0; j < dts.length; j++) {
            if (dts[j].toString().endsWith("=" + destination) || dts[j].toString().contains("=" + destination + ",")) {
                if(removeDurableTopicSubscriber(amqbc, dts[j], timeout))
                    ok++;
                else
                    failed++;
            }
        }
        System.out.print("[ Durable Subs: " + ok + " OK, " + failed + " FAILED ]");
    }

    /**
     * Remove Durable Topic Subscriber.
     *
     * @param name The ObjectName representing the durable topic subscriber.
     */
    private boolean removeDurableTopicSubscriber(AMQBrokerConnection amqbc, ObjectName name, long timeout) {
        try {
            amqbc.invoke(name, "destroy", null, null, timeout);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Try to match one string with the strings in a List.
     * Return true if original match one candidate.
     *
     * @param original The string to match
     * @param candidates The candidates to try with
     */
    private boolean destinationMatchOne(String name, boolean isQueue, List<String> candidates) {
        final String one = (isQueue ? "queue" : "topic") + "://" + name;
        final String two = (isQueue ? "/queue/" : "/topic/") + name;
        for (String el : candidates) {
            if (one.matches(el) || two.matches(el)) return true;
        }
        return false;
    }

}
