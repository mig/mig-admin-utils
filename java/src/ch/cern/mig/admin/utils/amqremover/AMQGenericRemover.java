/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ReflectionException;
import javax.script.ScriptException;

/**
 * <p>
 * ActiveMQ generic remover via JMX.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public abstract class AMQGenericRemover {

    //
    // private members for internal use (connection/username/passwd...)
    //
    public static int INTERACTIVE_MODE = 0;
    public static int NOACTION_MODE = 1;
    public static int FORCE_MODE = 2;
    protected int mode = INTERACTIVE_MODE;
    public List<String> columns = new ArrayList<String>();
    private boolean custom_columns = false;
    public static List<String> supported_columns = Arrays.asList("entity", "host", "string");
    public static List<String> default_columns = Arrays.asList("entity", "string");
    public static Map<String, String> headers_title = new HashMap<String, String>();
    static {
        Map<String, String> map = headers_title;
        map.put("id", "ID");
        map.put("entity", "ENTITY");
        map.put("host", "HOST");
        map.put("string", "VALUE");
    }

    /** Constructor.
     *
     * @param mode The removal mode
     */
    public AMQGenericRemover(int mode) {
        this.mode = mode;
        columns.addAll(default_columns);
    }

    /**
     * @param amqbc
     * @param filter
     * @param condition
     * @return Returns a list of ObjectName to remove
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     * @throws ScriptException
     */
    public abstract List<EnrichedObjectName> getObjects(AMQBrokerConnection amqbc, List<String> filter, String condition)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, ScriptException;

    /**
     * @param objects
     */
    public void sortObjects(List<EnrichedObjectName> objects){
        Collections.sort(objects);
    }

    /**
     * Print the headers for the output
     */
    public void printHeaders() {
        for(String header : columns){
            System.out.print(header);
        }
        System.out.println();
    }

    /**
     * Add header at a given index
     */
    public void addColumn(int index, String element) {
        columns.add(index, element);
    }

    /**
     * Remove specified header.
     */
    public void removeColumn(String element) {
        columns.remove(element);
    }

    /**
     * Return the headers values.
     */
    public String[] getHeadersValues() {
        String[] result = new String[columns.size()];
        for(int i = 0; i < columns.size(); i++){
            result[i] = headers_title.get(columns.get(i));
        }
        return result;
    }

    /**
     * If the columns have been set to custom values.
     *
     * @return true or false if columns have been customized.
     */
    public boolean isCustomColumns(){
        return custom_columns;
    }

    /**
     * Parse output format and set columns.
     * @throws Exception
     *
     */
    public void setOutputFormat(String output) throws Exception {
        if(output == null || output.length() == 0) return;
        custom_columns = true;
        columns = new ArrayList<String>();
        String[] items = output.split(",");
        for (String item : items) {
            if(supported_columns.contains(item.toLowerCase()))
                columns.add(item.toLowerCase());
            else
                throw new Exception(item + " not valid as output field.");
        }
        if(columns.isEmpty())
            throw new Exception("No valid output field specified.");
    }

    /** Remove a destination if it needs to be removed.
     * @param timeout
     * @throws RemoverException
     *
     */
    public abstract void removeObject(AMQBrokerConnection amqbc, EnrichedObjectName object, long timeout) throws RemoverException;

    /**
     * Get an attribute from a list.
     *
     * @param attributeList The list of attributes
     * @param index The index to return
     */
    public String getValueFromAttribute(AttributeList attributeList, int index) {
        String element;
        try{
            element = attributeList.get(index).toString();
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
        String[] sp = element.split("=");
        if (sp.length == 2)
            return sp[1].trim();
        return "";
    }

    /**
     * Get item default values.
     *
     * @param amqbc The AMQBrokerConnection item representing the broker related to the object.
     * @param object The object to return the information for.
     */
    public String[] getItemValues(AMQBrokerConnection amqbc, EnrichedObjectName object) {
        return getItemValues(amqbc, object, columns);
    }

    /**
     * Get item values.
     *
     * @param amqbc The AMQBrokerConnection item representing the broker related to the object.
     * @param object The object to return the information for.
     * @param attributes The attributes to include in the result.
     */
    public String[] getItemValues(AMQBrokerConnection amqbc, EnrichedObjectName object, List<String> attributes) {
        String[] result = new String[attributes.size()];
        for(int i = 0; i < attributes.size(); i++){
            if(attributes.get(i).equals("entity"))
                result[i] = amqbc.entity;
            else if (attributes.get(i).equals("host"))
                result[i] = amqbc.hostname;
            else if (attributes.get(i).equals("string"))
                result[i] = amqbc.toString();
            else
                result[i] = "" + object.getExtra(attributes.get(i));
        }
        return result;
    }

}
