/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.script.ScriptException;

import ch.cern.mig.common.utils.ExpEval;
import ch.cern.mig.common.utils.NetworkUtils;

/**
 * <p>
 * Remove ActiveMQ connections via JMX.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class ConnectionRemover extends AMQGenericRemover {

    public List<String> supported_columns_o = Arrays.asList("entity", "host", "subscriptions", "port", "address", "hostname");
    public List<String> default_columns_o = Arrays.asList("entity", "subscriptions", "port", "address", "hostname");
    private static final Map<String, String> headers_title_o = new HashMap<String, String>();

    static {
        Map<String, String> map = headers_title_o;
        map.put("id", "ID");
        map.put("entity", "ENTITY");
        map.put("host", "HOST");
        map.put("subscriptions", "SUBSCRIPTIONS");
        map.put("port", "PORT");
        map.put("address", "ADDRESS");
        map.put("hostname", "HOSTNAME");
    }

    public class SortConnections implements Comparator<EnrichedObjectName>{

        public int compare(EnrichedObjectName o1, EnrichedObjectName o2) {
            final String name1 = (String) o1.getExtra("name");
            final String name2 = (String) o2.getExtra("name");
            return name1.compareTo(name2);
        }
    }

    /** Constructor.
     *
     * @param mode The removal mode
     */
    public ConnectionRemover (int mode) {
        super(mode);
        supported_columns = supported_columns_o;
        default_columns = default_columns_o;
        columns = new ArrayList<String>(default_columns);
        headers_title.putAll(headers_title_o);
    }

    /**
     * @return Returns a list of all Connections to remove from AMQ JMX domain
     *
     * @param amqbc The broker connection
     *
     * @throws AttributeNotFoundException
     * @throws InstanceNotFoundException
     * @throws MalformedObjectNameException
     * @throws MBeanException
     * @throws ReflectionException
     * @throws NullPointerException
     * @throws IOException
     * @throws ScriptException
     */
    public List<EnrichedObjectName> getObjects(AMQBrokerConnection amqbc, List<String> filter, String condition)
        throws AttributeNotFoundException, InstanceNotFoundException, MBeanException,
               ReflectionException, IOException, MalformedObjectNameException, ScriptException {
        String objectName = amqbc.isOldType() ? "org.apache.activemq:BrokerName=" + amqbc.brokername + ",Type=Connection,Connection=*,*" :
            "org.apache.activemq:type=Broker,brokerName=" + amqbc.brokername + ",connector=clientConnectors,connectorName=*,connectionName=*,*";
        List<ObjectName> ons = new ArrayList<ObjectName>(amqbc.mbsc.queryNames(
                                                                               new ObjectName(objectName),
                                                                               null));
        List<EnrichedObjectName> result = new ArrayList<EnrichedObjectName>();
        Pattern p = Pattern.compile("//(.*):(.*)");
        for (ObjectName on : ons) {
            String remoteAddress = "";
            try {
                remoteAddress = amqbc.mbsc.getAttribute(on, "RemoteAddress").toString();
            } catch (InstanceNotFoundException e) {
                continue;
            }
            Matcher m = p.matcher(remoteAddress);
            if (! m.find()) continue;
            String ip = m.group(1);
            String port = m.group(2);
            if (! (objectNameMatchesOne(ip + ":" + port, filter) || objectNameMatchesOne(NetworkUtils.getHostname(ip) + ":" + port, filter))) continue;
            EnrichedObjectName eon = new EnrichedObjectName(on.toString());
            Map <String, String> params = new HashMap<String, String>();
            if (columns.contains("subscriptions")) {
                int subs = getSubscriptionsCount(amqbc, on);
                params.put("subscriptions", "" + subs);
                eon.putExtra("subscriptions", subs);
            }
            if (! ExpEval.eval(condition, params)) continue;
            eon.putExtra("name", ip + ":" + port);
            if (columns.contains("port")) eon.putExtra("port", port);
            if (columns.contains("address")) eon.putExtra("address", ip);
            if (columns.contains("hostname")) eon.putExtra("hostname", NetworkUtils.getHostname(ip));
            result.add(eon);
        }
        sortObjects(result);
        return result;
    }

    /**
     * @param objects
     */
    public void sortObjects(List<EnrichedObjectName> objects) {
        Collections.sort(objects, new SortConnections());
    }

    private int getSubscriptionsCount(AMQBrokerConnection amqbc, ObjectName on) throws MalformedObjectNameException, NullPointerException, IOException {
        int subs = 0;
        Pattern p = amqbc.isOldType() ? Pattern.compile(".*Connection=(.*)") : Pattern.compile(".*connectionName=(.*)");
        Matcher m = p.matcher(on.toString());
        if (! m.find()) return subs;
        String conn = m.group(1);
        String objectName = amqbc.isOldType() ? "org.apache.activemq:BrokerName=" + amqbc.brokername + ",Type=Subscription,clientId=" + conn + ",*" :
            "org.apache.activemq:type=Broker,brokerName=" + amqbc.brokername + ",endpoint=Consumer,clientId=" + conn + ",*";
        List<ObjectName> sub = new ArrayList<ObjectName>(amqbc.mbsc.queryNames(
                                                                               new ObjectName(objectName), null));
        subs += sub.size();
        return subs;
    }

    private boolean objectNameMatchesOne(String ip, List<String> filter) {
        for(String el: filter) {
            if (ip.toString().matches(el)) return true;
        }
        return false;
    }

    /** Remove an object if it needs to be removed.
     *
     * @param amqbc The broker connection
     * @param object An ObjectName
     * @throws RemoverException
     */
    public void removeObject(AMQBrokerConnection amqbc, EnrichedObjectName object, long timeout) throws RemoverException {
        try{
            amqbc.invoke(new ObjectName(object.toString()), "stop", null , null, timeout);
        } catch(TimeoutException e) {
            throw new RemoverException("Timeout removing: " + object);
        } catch (Exception e) {
            throw new RemoverException("Error removing " + object + ": " + e);
        }
    }

}
