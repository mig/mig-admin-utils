/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.admin.utils.amqremover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 * <p>
 * ActiveMQ broker connection via JMX.<br><br>
 * </p>
 * @author Massimo Paladin <Massimo.Paladin@cern.ch>
 *
 */
public class AMQBrokerConnection {
    public static final String TYPE_UNKNOWN = "unknown";
    public static final String TYPE_OLD = "old"; // < 5.8
    public static final String TYPE_NEW = "new"; // >= 5.8
    public static final Pattern AMQ_PATTERN = Pattern.compile("[:,][bB]rokerName=([a-zA-Z0-9\\.\\:\\-\\_]+),");

    public String brokername;
    public Hashtable<String,Object> credentials;
    public String entity;
    public String hostname;
    public JMXConnector jmxc = null;
    public MBeanServerConnection mbsc = null;
    public int port;
    private ExecutorService executor;
    private String type = TYPE_UNKNOWN;

    /** Constructor.
     *
     * @param hostname The hostname.
     * @param port The port.
     * @param brokername The brokername.
     * @param credentials The credentials to use for the connection.
     * @throws IOException
     */
    public AMQBrokerConnection(String hostname, int port, String brokername, Hashtable<String,Object> credentials) {
        this(hostname, hostname, port, brokername, credentials);
    }

    /** Constructor.
     *
     * @param entity A readable name for the host.
     * @param hostname The hostname.
     * @param port The port.
     * @param brokername The brokername.
     * @param credentials The credentials to use for the connection.
     */
    public AMQBrokerConnection(String entity, String hostname, int port, String brokername, Hashtable<String,Object> credentials) {
        this.entity = entity;
        this.hostname = hostname;
        this.port = port;
        this.brokername = brokername;
        this.credentials = credentials;
    }

    /**
     * Tell the type of the broker.
     */
    public String getType() {
        return type;
    }

    /**
     * Tell if broker is old type, version earlier than 5.8.
     */
    public boolean isOldType() {
        return type.equals(TYPE_OLD) && ! type.equals(TYPE_UNKNOWN);
    }

    /** Establishes the connection to the JMX management interface.
     *
     * @throws IOException
     * @throws SecurityException
     * @throws MalformedObjectNameException
     * @throws Exception
     */
    public void connect() throws IOException, SecurityException, MalformedObjectNameException, Exception {
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + hostname + ":" + port + "/jmxrmi");
        jmxc = JMXConnectorFactory.connect(url, credentials.isEmpty() ? null : credentials);
        mbsc = jmxc.getMBeanServerConnection();
        executor = Executors.newCachedThreadPool();
        // try to connect with new ActiveMQ style
        List<ObjectName> brokerObj = new ArrayList<ObjectName>(mbsc.queryNames(
            new ObjectName("org.apache.activemq:type=Broker,service=Health,*"), null));
        if (brokerObj.size() >= 1) {
            Matcher m = AMQ_PATTERN.matcher(brokerObj.get(0).toString());
            if (m.find()) {
                if (brokername == null) {
                    brokername = m.group(1);
                } else if (!brokername.equals(m.group(1))) {
                    throw new Exception("Unexpected broker name: " + m.group(1));
                }
                type = TYPE_NEW;
                return;
            }
        }
        // try to connect with old ActiveMQ style
        brokerObj = new ArrayList<ObjectName>(mbsc.queryNames(
            new ObjectName("org.apache.activemq:Type=Broker,*"), null));
        if (brokerObj.size() >= 1) {
            Matcher m = AMQ_PATTERN.matcher(brokerObj.get(0).toString());
            if (m.find()) {
                if (brokername == null) {
                    brokername = m.group(1);
                } else if (!brokername.equals(m.group(1))) {
                    throw new Exception("Unexpected broker name: " + m.group(1));
                }
                type = TYPE_OLD;
                return;
            }
        }
        // so far so bad
        throw new Exception("Unknown broker type");
    }

    /** Closes the JMX connection we keep.
     *
     */
    public void disconnect() {
        if (jmxc == null) return;
        executor.shutdownNow();
        try {
            jmxc.close();
        } catch (IOException ex) {
            System.err.println("Error closing connection with " + entity);
        }
    }

    /** Return connection status.
     *
     */
    public boolean isConnected(){
        return jmxc != null && mbsc != null;
    }

    /** Invoke MBeanServerConncetion operation.
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     *
     */
    public Object invoke(ObjectName name, String operationName, Object[] params, String[] signature) throws
        InstanceNotFoundException, MBeanException, ReflectionException, IOException, TimeoutException{
        return mbsc.invoke(name, operationName, params, signature);
    }

    /**
     * Custom Callable
     *
     */
    public class MyCallable implements Callable<Object> {
        private ObjectName name;
        private String operationName;
        private Object[] params;
        private String[] signature;

        public MyCallable(ObjectName name, String operationName, Object[] params, String[] signature){
            this.name = name;
            this.operationName = operationName;
            this.params = params;
            this.signature = signature;
        }

        public Object call() {
            try {
                return mbsc.invoke(name, operationName, params, signature);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /** Invoke MBeanServerConncetion operation with timeout.
     * @throws IOException
     * @throws ReflectionException
     * @throws MBeanException
     * @throws InstanceNotFoundException
     * @throws TimeoutException
     * @throws ExecutionException
     * @throws InterruptedException
     *
     */
    public Object invoke(ObjectName name, String operationName, Object[] params, String[] signature, long timeout) throws
        InterruptedException, ExecutionException, TimeoutException {
        Callable<Object> task = new MyCallable(name, operationName, params, signature);
        Future<Object> future = executor.submit(task);
        Object result = future.get(timeout, TimeUnit.MILLISECONDS);
        return result;
    }
}
