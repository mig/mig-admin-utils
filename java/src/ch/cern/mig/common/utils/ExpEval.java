package ch.cern.mig.common.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ExpEval {
	private static ScriptEngineManager mgr = new ScriptEngineManager();
	private static ScriptEngine engine = mgr.getEngineByName("JavaScript");
	
	public ExpEval () {
		
	}
	
	public static boolean eval(String expr) throws ScriptException {
		return eval(expr, null);
	}
	
	public static boolean eval(String expr, Map<String, String> values) throws ScriptException {
		if (expr == null || expr.trim().length() == 0) return true;
		String result = null;
		
		if (values != null) expr = replaceAll(expr, values);
		
		result = engine.eval(expr).toString();
		if (result.equals("true"))
			return true;
		else if (result.equals("false"))
			return false;
		else
			throw new ScriptException("Condition expression should return either true or false, value returned: " + result);
	}

	private static String replaceAll(String expr, Map<String, String> values) {
		for(Iterator<Map.Entry<String, String>> it = values.entrySet().iterator(); it.hasNext();){
			Map.Entry<String, String> entry = it.next();
			expr = expr.replace(entry.getKey(), entry.getValue());
		}
		return expr;
	}
	
	
	public static void main(String[] args) throws ScriptException {
		Map <String, String> params = new HashMap<String, String>();
		params.put("count", "5");
		System.out.println(ExpEval.eval("count-2 != 3", params));
	}
}
