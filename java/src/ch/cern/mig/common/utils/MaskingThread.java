/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.cern.mig.common.utils;

/**
 * This class attempts to erase characters echoed to the console.
 */
class MaskingThread extends Thread {
	private volatile boolean stop;
	private char echochar = '*';

	/**
	 *@param prompt The prompt displayed to the user
	 */
	public MaskingThread(String prompt) {
		System.out.print(prompt);
	}

	/**
	 * Begin masking until asked to stop.
	 */
	public void run() {

		int priority = Thread.currentThread().getPriority();
		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

		try {
			stop = true;
			while(stop) {
				System.out.print("\010" + echochar);
				try {
					Thread.currentThread();
					// attempt masking at this rate
					Thread.sleep(1);
				}catch (InterruptedException iex) {
					Thread.currentThread().interrupt();
					return;
				}
			}
		} finally { // restore the original priority
			Thread.currentThread().setPriority(priority);
		}
	}

	/**
	 * Instruct the thread to stop masking.
	 */
	public void stopMasking() {
		this.stop = false;
	}
}
