package ch.cern.mig.common.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class NetworkUtils {
	private static Map<String, String> hostnames = new HashMap<String, String>();
	
	/**
	 * @param ip address
	 * @return hostname
	 */
	public static String getHostname(String ip) {
		String hostname = hostnames.get(ip);
		if (hostname != null) return hostname;
		try {
			hostname = InetAddress.getByName(ip).getHostName();
		} catch (UnknownHostException ex) {
			hostname = "error: " + ex.getMessage();
		}
		hostnames.put(ip, hostname);
		return hostname.toLowerCase();
	}

}
