#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: s3t                                                                    #
#                                                                              #
# Description: STOMP Stress and Speed Tester                                   #
#                                                                              #
#-##############################################################################

# $Revision: 1021 $

#
# modules
#

use strict;
use warnings qw(FATAL all);
use 5.005; # need the four-argument form of substr()
use Authen::Credential qw();
use Getopt::Long qw(GetOptions);
use IO::Select qw();
use IO::Socket::INET qw();
use No::Worries::Die qw(dief handler);
use No::Worries::File qw(file_write);
use No::Worries::Log qw(log_debug log_filter);
use Pod::Usage qw(pod2usage);
use Time::HiRes qw();

#
# global variables
#

our(%Option, %Time, %Msgs, %Bytes, %Profile, @RndSize, @RndSleep);

#
# initialization
#

sub init () {
    $| = 1;
    $Option{bufsize} = 8192;
    $Option{debug} = 0;
    $Option{interval} = 1;
    $Option{parallel} = 1;
    $Option{retry} = 0;
    $Option{size} = 0;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option,
        "auth=s",
        "broker=s",
        "bufsize=i",
        "content-length",
        "count=i",
        "debug|d+",
        "destination=s",
        "drain+",
        "duration=i",
        "header=s\@",
        "help|h|?",
        "host=s",
        "interval=i",
        "manual|m",
        "parallel=i",
        "parse",
        "profile-messages=s",
        "profile-bytes=s",
        "retry=i",
        "rndsize=i",
        "rndsleep=i",
        "role=s",
        "size=i",
        "sleep=f",
        "version=s",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) if @ARGV;
    log_filter("debug") if $Option{debug};
}

#
# checking
#

sub check () {
    my($auth);

    # check mandatory options
    foreach my $name (qw(role broker destination)) {
        next if $Option{$name};
        dief("mandatory option not set: --%s", $name);
    }
    # check numbers
    foreach my $name (qw(bufsize count duration interval parallel retry
                         rndsize rndsleep size)) {
        next unless defined($Option{$name});
        next if $Option{$name} > 0;
        next if $Option{$name} == 0 and $name =~ /^(count|retry|size)$/;
        dief("invalid number for --%s: %s", $name, $Option{$name});
    }
    # check broker URI
    if ($Option{broker} =~
        /^(tcp|ssl|stomp|stomp\+ssl):\/\/([\w\-\.]+):(\d+)$/) {
        $Option{_host} = $2;
        $Option{_port} = $3;
    } else {
        dief("invalid broker URI: %s", $Option{broker});
    }
    # check authentication
    if ($Option{auth}) {
        $auth = Authen::Credential->parse($Option{auth});
        $auth->check();
        if ($auth->scheme() eq "plain") {
            $Option{login} = $auth->name();
            $Option{passcode} = $auth->pass();
        } elsif ($auth->scheme() eq "x509") {
            $Option{ssl} = $auth->prepare("IO::Socket::SSL");
        } elsif ($auth->scheme() ne "none") {
            dief("unsupported authentication scheme: %s", $auth->scheme());
        }
    }
    # check SSL availability
    if ($Option{ssl}) {
        eval { require IO::Socket::SSL };
        dief("cannot load IO::Socket::SSL: %s", $@) if $@;
    }
}

#
# return a random integer between 0 and 2*size, with a normal distribution
#

sub rndint ($) {
    my($size) = @_;
    my($rnd);

    # see Irwin-Hall in http://en.wikipedia.org/wiki/Normal_distribution
    $rnd = rand(1) + rand(1) + rand(1) + rand(1) + rand(1) + rand(1) +
           rand(1) + rand(1) + rand(1) + rand(1) + rand(1) + rand(1);
    return(int($rnd * $size / 6 + 0.5));
}

#
# setup the random arrays
#

sub rndsetup () {
    my($total, $rnd);

    # @RndSize
    $Option{rndsize} = 0 unless $Option{size};
    if ($Option{rndsize}) {
        $total = 0;
        foreach my $i (1 .. $Option{rndsize}) {
            $rnd = rndint($Option{size});
            $total += $rnd;
            push(@RndSize, $rnd);
        }
        log_debug("generated %d message sizes (avg %d)",
                  $Option{rndsize}, int($total/$Option{rndsize} + 0.5));
    }
    # @RndSleep
    $Option{rndsleep} = 0 unless $Option{sleep};
    if ($Option{rndsleep}) {
        $total = 0;
        foreach my $i (1 .. $Option{rndsleep}) {
            $rnd = rndint($Option{rndsleep}) / $Option{rndsleep}
                   * $Option{sleep};
            $total += $rnd;
            push(@RndSleep, $rnd);
        }
        log_debug("generated %d sleep times (avg %f)",
                  $Option{rndsleep}, $total/$Option{rndsleep});
    }
}

#
# keep track of statistics
#

sub init_stats () {
    $Profile{messages} = $Profile{bytes} = "";
    $Msgs{stat}  = $Msgs{total}  = 0;
    $Bytes{stat} = $Bytes{total} = 0;
    $Time{stat}  = $Time{start}  = Time::HiRes::time();
}

sub update_stats ($) {
    my($messages) = @_;

    $Time{period}   = $Time{now} - $Time{stat};
    $Time{elapsed}  = $Time{now} - $Time{start};
    $Msgs{period}   = $Msgs{stat}   / $Time{period};
    $Msgs{average}  = $Msgs{total}  / $Time{elapsed};
    $Bytes{period}  = $Bytes{stat}  / $Time{period};
    $Bytes{average} = $Bytes{total} / $Time{elapsed};
    if ($messages) {
        log_debug("msg/s = %d (avg %d) - kB/s = %d (avg %d)",
                  int(0.5 + $Msgs{period}),
                  int(0.5 + $Msgs{average}),
                  int(0.5 + $Bytes{period} / 1024),
                  int(0.5 + $Bytes{average} / 1024),
        );
    } else {
        log_debug("kB/s = %d (avg %d)",
                  int(0.5 + $Bytes{period} / 1024),
                  int(0.5 + $Bytes{average} / 1024),
        );
    }
    $Profile{messages} .= $Time{stat} . " " . $Msgs{period} . "\n"
        if defined($Option{"profile-messages"});
    $Profile{bytes} .= $Time{stat} . " " . $Bytes{period} . "\n"
        if defined($Option{"profile-bytes"});
    $Time{stat} = $Time{now};
    $Msgs{stat} = $Bytes{stat} = 0;
}

sub report_stats ($$) {
    my($messages, $what) = @_;

    $Time{now}      = Time::HiRes::time();
    $Time{period}   = $Time{now} - $Time{stat}  || 0.0001;
    $Time{elapsed}  = $Time{now} - $Time{start} || 0.0001;
    $Msgs{period}   = $Msgs{stat}   / $Time{period};
    $Msgs{average}  = $Msgs{total}  / $Time{elapsed};
    $Bytes{period}  = $Bytes{stat}  / $Time{period};
    $Bytes{average} = $Bytes{total} / $Time{elapsed};
    if ($messages) {
        printf("%s %d messages in %.2f seconds (%d msg/s, %d kB/s)\n",
               $what, $Msgs{total}, $Time{elapsed},
               int(0.5 + $Msgs{average}),
               int(0.5 + $Bytes{average} / 1024),
        );
    } else {
        printf("%s %d MB in %.2f seconds (%d kB/s)\n",
               $what, int(0.5 + $Bytes{total} / 1024 / 1024), $Time{elapsed},
               int(0.5 + $Bytes{average} / 1024),
        );
    }
    # save profiling data
    if (defined($Option{"profile-messages"})) {
        if ($Msgs{period}) {
            # processed messages during the last period
            $Profile{messages} .= $Time{stat} . " " . $Msgs{period} . "\n";
            $Profile{messages} .= $Time{now} . " 0\n";
        } else {
            # no new messages since $Time{stat}
            $Profile{messages} .= $Time{stat} . " 0\n";
        }
        file_write($Option{"profile-messages"}, data => $Profile{messages});
    }
    if (defined($Option{"profile-bytes"})) {
        if ($Bytes{period}) {
            # processed bytes during the last period
            $Profile{bytes} .= $Time{stat} . " " . $Bytes{period} . "\n";
            $Profile{bytes} .= $Time{now} . " 0\n";
        } else {
            # no new bytes since $Time{stat}
            $Profile{bytes} .= $Time{stat} . " 0\n";
        }
        file_write($Option{"profile-bytes"}, data => $Profile{bytes});
    }
}

#
# send one (small) frame to the server
#

sub do_send ($$$) {
    my($socket, $select, $frame) = @_;
    my($done);

    dief("cannot write to server!")
        unless $select->can_write(60);
    $done = syswrite($socket, $frame);
    dief("cannot syswrite: %s", $!)
        unless defined($done);
    dief("partial syswrite: %d < %d", $done, length($frame))
        unless $done == length($frame);
}

#
# connect to the STOMP server and subscribe if needed
#

sub do_connect () {
    my(%option, $auth, $socket, $select, $frame);

    # TCP level connection
    %option = (
        Proto    => "tcp",
        PeerAddr => $Option{_host},
        PeerPort => $Option{_port},
    );
    if ($Option{ssl}) {
        $socket = IO::Socket::SSL->new(%option, %{ $Option{ssl} });
        dief("cannot ssl connect to %s:%d: %s",
                  $Option{_host}, $Option{_port}, IO::Socket::SSL::errstr())
            unless $socket;
    } else {
        $socket = IO::Socket::INET->new(%option);
        dief("cannot tcp connect to %s:%d: %s",
             $Option{_host}, $Option{_port}, $@) unless $socket;
    }
    log_debug("connected to %s:%d with socket %d",
              $Option{_host}, $Option{_port}, fileno($socket));
    $select = IO::Select->new();
    $select->add($socket);
    # STOMP level connection
    $frame = "CONNECT\n";
    $frame .= "login:$Option{login}\n" if defined($Option{login});
    $frame .= "passcode:$Option{passcode}\n" if defined($Option{passcode});
    $frame .= "accept-version:$Option{version}\n" if defined($Option{version});
    $frame .= "host:$Option{host}\n" if defined($Option{host});
    $frame .= "\n";
    $frame .= "\0";
    do_send($socket, $select, $frame);
    dief("cannot read from server!") unless $select->can_read(60);
    dief("cannot sysread from server!") unless sysread($socket, $frame, 8192);
    dief("STOMP connection failed!") unless $frame =~ /^CONNECTED\n/;
    if ($frame =~ /^session\s*:\s*(\S+)\s*$/m) {
        log_debug("session is %s", $1);
    }
    if ($frame =~ /^version\s*:\s*(\S+)\s*$/m) {
        log_debug("version is %s", $1);
    }
    if ($frame =~ /^server\s*:\s*(\S+)\s*$/m) {
        log_debug("server is %s", $1);
    }
    # subscription
    if ($Option{role} ne "producer") {
        $frame = "SUBSCRIBE\n";
        $frame .= "destination:$Option{destination}\n";
        $frame .= "id:0\n" if $Option{version} and $Option{version} eq "1.1";
        $frame .= "\n";
        $frame .= "\0";
        do_send($socket, $select, $frame);
        log_debug("subscribed to %s", $Option{destination});
    }
    # so far so good
    return($socket);
}

sub do_connect_wrapped () {
    my($socket);

    eval { $socket = do_connect() };
    while ($@ and $@ =~ /Connection timed out/ and $Option{retry}-- > 0) {
        warn($@);
        eval { $socket = do_connect() };
    }
    die($@) if $@;
    return($socket);
}

#
# producer role
#

sub do_producer () {  ## no critic 'ProhibitExcessComplexity'
    my($stats, $frame, $size, $select, $done, $running);
    my(%buffer, $length, $todo, $count, $rndframe);

    # setup
    rndsetup();
    $stats = $Option{debug}
        || defined($Option{"profile-messages"})
        || defined($Option{"profile-bytes"});
    $frame  = "SEND\n";
    $frame .= "destination:$Option{destination}\n";
    foreach my $line (@{ $Option{header} }) {
        dief("invalid header line: %s", $line) unless $line =~ /:/;
        $frame .= "$line\n";
    }
    $frame .= "\n";
    if ($Option{rndsize}) {
        log_debug("frame fixed header size is %d", length($frame));
    } else {
        substr($frame, 5, 0, "content-length:$Option{size}\n")
            if $Option{"content-length"};
        $frame .= "X" x $Option{size} if $Option{size};
        $frame .= "\0";
        $size = length($frame);
        log_debug("frame total size is %d", $size);
    }
    $select = IO::Select->new();
    foreach my $i (1 .. $Option{parallel}) {
        $todo = do_connect_wrapped();
        $select->add($todo);
        $buffer{"$todo"} = "";
    }
    # run
    init_stats();
    $Msgs{buffer} = 0;
    $running = 1;
    $running = 0 if defined($Option{count}) and $Option{count} <= 0;
    local $SIG{INT} = sub { print("\n"); $running = 0; };
    while ($running) {
        foreach my $socket ($select->can_write(0.0001)) {
            # add frames to the buffer if needed and possible
            $length = length($buffer{"$socket"});
            if ($Option{rndsize}) {
                while (1) {
                    last if $Option{count} and
                        $Msgs{total} + $Msgs{buffer} >= $Option{count};
                    last if $length >= $Option{bufsize};
                    $size = $RndSize[int(rand($Option{rndsize}))];
                    $rndframe = $frame;
                    substr($rndframe, 5, 0, "content-length:$size\n")
                        if $Option{"content-length"};
                    $rndframe .= "X" x $size;
                    $rndframe .= "\0";
                    $buffer{"$socket"} .= $rndframe;
                    $length += length($rndframe);
                    $Msgs{buffer}++;
                }
            } else {
                $count = int(($Option{bufsize} - $length + $size - 1) / $size);
                if ($count) {
                    if ($Option{count}) {
                        $todo = $Option{count} - $Msgs{total} - $Msgs{buffer};
                        $count = $todo if $todo < $count;
                    }
                    if ($count > 0) {
                        $buffer{"$socket"} .= $frame x $count;
                        $length += $size * $count;
                        $Msgs{buffer} += $count;
                    }
                }
            }
            # send data if we have some
            if ($length) {
                $done = syswrite($socket, $buffer{"$socket"});
                dief("cannot syswrite: %s", $!) unless defined($done);
                if ($Option{sleep}) {
                    if ($Option{rndsleep}) {
                        $todo = $RndSleep[int(rand($Option{rndsleep}))];
                    } else {
                        $todo = $Option{sleep};
                    }
                    Time::HiRes::sleep($todo);
                }
                if ($done) {
                    $count = substr($buffer{"$socket"}, 0, $done) =~ tr/\0/\0/;
                    substr($buffer{"$socket"}, 0, $done, "");
                    $Bytes{stat} += $done;
                    $Bytes{total} += $done;
                    $Msgs{stat} += $count;
                    $Msgs{total} += $count;
                    $Msgs{buffer} -= $count;
                }
            }
        }
    } continue {
        $Time{now} = Time::HiRes::time() if $stats or $Option{duration};
        update_stats(1)
            if $stats and $Time{now} - $Time{stat} >= $Option{interval};
        last if $Option{count}
            and $Msgs{total} >= $Option{count};
        last if $Option{duration}
            and $Time{now} - $Time{start} >= $Option{duration};
    }
    # report & save profiling data
    report_stats(1, "sent");
    # check if we received something from the server
    foreach my $socket ($select->can_read(0.1)) {
        dief("cannot sysread: %s", $!) unless sysread($socket, $frame, 8192);
        $frame =~ s/\0/\\0/g;
        warn("* unexpected frame received from server:\n");
        print("$frame\n");
    }
}

#
# consumer role
#

sub do_consumer () {  ## no critic 'ProhibitExcessComplexity'
    my($stats, $select, $frame, $done, $count, $running, $active);

    # setup
    rndsetup();
    $stats = $Option{debug} || $Option{drain}
        || defined($Option{"profile-messages"})
        || defined($Option{"profile-bytes"});
    $Option{drain} *= $Option{interval} if $Option{drain};
    $select = IO::Select->new();
    foreach my $i (1 .. $Option{parallel}) {
        $select->add(do_connect_wrapped());
    }
    # run
    init_stats();
    $Time{active} = $Time{start};
    $running = 1;
    $running = 0 if defined($Option{count}) and $Option{count} <= 0;
    local $SIG{INT} = sub { print("\n"); $running = 0; };
    while ($running) {
        $active = 0;
        foreach my $socket ($select->can_read(0.0001)) {
            $done = sysread($socket, $frame, $Option{bufsize});
            dief("cannot sysread: %s", $!) unless defined($done);
            dief("cannot sysread: %s", "EOF") unless $done;
            if ($Option{sleep}) {
                if ($Option{rndsleep}) {
                    Time::HiRes::sleep($RndSleep[int(rand($Option{rndsleep}))]);
                } else {
                    Time::HiRes::sleep($Option{sleep});
                }
            }
            if ($Option{parse}) {
                # rudimentary frame parsing: we count the number of NULL characters,
                # this works only with text messages
                $count = $frame =~ tr/\0/\0/;
                $Msgs{stat} += $count;
                $Msgs{total} += $count;
            }
            $Bytes{total} += $done;
            $Bytes{stat} += $done;
            $frame = "";
            $active++;
        }
    } continue {
        $Time{now} = Time::HiRes::time() if $stats or $Option{duration};
        update_stats($Option{parse})
            if $stats and $Time{now} - $Time{stat} >= $Option{interval};
        if ($Option{drain}) {
            if ($active) {
                $Time{active} = $Time{now};
            } else {
                last if $Time{now} - $Time{active} > $Option{drain};
            }
        }
        if ($Option{parse}) {
            last if $Option{count}
                and $Msgs{total} >= $Option{count};
        } else {
            last if $Option{count}
                and $Bytes{total} >= $Option{count};
        }
        last if $Option{duration}
            and $Time{now} - $Time{start} >= $Option{duration};
    }
    # report & save profiling data
    report_stats($Option{parse}, "received");
}

#
# just do it ;-)
#

init();
check();
if ($Option{role} eq "producer") {
    foreach my $name (qw(drain parse)) {
        dief("cannot use --%s with --role=%s!", $name, $Option{role})
            if $Option{$name};
    }
    do_producer();
} elsif ($Option{role} eq "consumer") {
    foreach my $name (qw(header rndsize size)) {
        dief("cannot use --%s with --role=%s!", $name, $Option{role})
            if $Option{$name};
    }
    $Option{parse} = 1 if defined($Option{"profile-messages"});
    do_consumer();
} else {
    dief("unexpected value for --role: %s", $Option{role});
}

__END__

=head1 NAME

s3t - STOMP Stress and Speed Tester

=head1 SYNOPSIS

B<s3t> --role producer --broker I<URI> --destination I<NAME> [I<OPTIONS>]

B<s3t> --role consumer --broker I<URI> --destination I<NAME> [I<OPTIONS>]

=head1 DESCRIPTION

B<s3t> can be used to (stress) test and measure the performance of a
STOMP server.

Depending on its role, it can either produce or consume messages, as
fast as it can.

In both cases, it directly interacts with the TCP socket and does not
use any fancy STOMP library. It does support authentication and SSL
but it does not support receipts, acknowledgments and transactions.

By default, the consumer simply receives and discards the data,
without even parsing the frames. Therefore, it cannot detect ERROR
frames or count the number of messages received. With the B<--parse>
option, it performs some B<rudimentary> parsing (in fact, it simply
counts the number of NULL character received) to be able to count
messages. Obviously, this only works with text messages.

By default, the program produces or consumes messages as fast as it
can. If this behaviour is not needed (e.g. to simulate a slow client),
there are two main options that can be used. First, B<--bufsize> can
be reduced to force the client to perform more read(2) or write(2)
system calls. Then, B<--sleep> (and its associated B<--rndsleep>) can
be used to force the program to sleep after each read(2) or write(2)
system call.

The B<--profile-bytes> and B<--profile-messages> options allow to save
performance data. Each line contains, space separated, the fractional
time and the speed (number of bytes or mssages per second) from this
time onwards.

=head1 OPTIONS

=over

=item B<--auth> I<STRING>

the authentication to use such as C<plain name=guest pass=guest>,
see L<Authen::Credential> for more information

=item B<--broker> I<URI>

the broker to connect to (mandatory)

=item B<--bufsize> I<INTEGER>

the buffer size for I/O operations, in bytes
(default: 8192)

=item B<--content-length>

for a producer: add the C<content-length> header line indicating the
length of the message body

=item B<--count> I<INTEGER>

for a producer: the number of messages to produce,
for a consumer: the number of messages (with B<--parse>) or bytes
(otherwise) to consume
(default: no limit)

=item B<--debug>, B<-d>

show debugging information, including live statistics

=item B<--destination> I<NAME>

the destination name (mandatory)

=item B<--drain>

for a consumer: stop as soon as no data has been received during a
complete time interval
(can be given multiple times to mean multiple time intervals)

=item B<--duration> I<INTEGER>

the running time in seconds
(default: no limit)

=item B<--header> I<STRING>

for a producer: add the given header line
(can be given multiple times)

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--host> I<STRING>

the host to use in the CONNECT frame

=item B<--interval> I<INTEGER>

the interval between printed statistics (in debug mode) or performance
data, in seconds (default: 1)

=item B<--manual>, B<-m>

show this manual

=item B<--parallel> I<INTEGER>

number of parallel connections to make to the server
(default: 1)

=item B<--parse>

for a consumer: rudimentary parse the data received to be able to
count messages

=item B<--profile-bytes> I<PATH>

save a profile of the bytes processed in the given file

=item B<--profile-messages> I<PATH>

save a profile of the messages processed in the given file

=item B<--retry> I<INTEGER>

in case of "connection timed out" error, number of retry attempts
(default: 0)

=item B<--rndsize> I<INTEGER>

for a producer: randomize the body sizes of the sent messages by using
this number of different sizes, with a normal distribution around the
value specified by the B<--size> option (default: 0)

=item B<--rndsleep> I<INTEGER>

randomize the sleep times, with a normal distribution around the value
specified by the B<--sleep> option (default: 0)

=item B<--role> B<producer>|B<consumer>

the role to play (mandatory)

=item B<--size> I<INTEGER>

for a producer: the body size of the sent messages (default: 0)

=item B<--sleep> I<NUMBER>

the time to sleep after each read(2) or write(2) system call, in
fractional seconds (default: 0)

=item B<--version> I<STRING>

the STOMP version to use, set it to C<1.1> to enable (partial) STOMP
1.1 support (default: 1.0)

=back

=head1 EXAMPLES

Send at most 10M messages with a 256 bytes body to a test topic and do
not run for more than 10 seconds:

  $ s3t --role producer --broker stomp://localhost:6163 \
    --destination /topic/test --size 256 \
    --count 10000000 --duration 10
  sent 292199 messages in 10.00 seconds (29220 msg/s, 8247 kB/s)

Send empty persistent messages forever (in fact, until ^C is typed),
with debugging:

  $ s3t --role producer --broker stomp://localhost:6163 \
    --header persistent:true --destination /topic/test --debug
  # ...-10:58:15 s3t: connected to localhost:6163
  # ...-10:58:15 s3t: session is ID:localhost-41281-4:2287418
  # ...-10:58:15 s3t: frame size is 33
  # ...-10:58:16 s3t: msg/s = 38433 (avg 38433) - kB/s = 1239 (avg 1239)
  # ...-10:58:17 s3t: msg/s = 39143 (avg 38788) - kB/s = 1261 (avg 1250)
  # ...-10:58:18 s3t: msg/s = 39172 (avg 38916) - kB/s = 1262 (avg 1254)
  # ...-10:58:19 s3t: msg/s = 39172 (avg 38980) - kB/s = 1262 (avg 1256)
  # ...-10:58:20 s3t: msg/s = 38583 (avg 38901) - kB/s = 1243 (avg 1254)
  # ...-10:58:21 s3t: msg/s = 38667 (avg 38862) - kB/s = 1246 (avg 1252)
  # ...-10:58:22 s3t: msg/s = 39165 (avg 38905) - kB/s = 1262 (avg 1254)
  # ...-10:58:23 s3t: msg/s = 39176 (avg 38939) - kB/s = 1263 (avg 1255)
  # ...-10:58:24 s3t: msg/s = 39169 (avg 38964) - kB/s = 1262 (avg 1256)
  ^C
  sent 381718 messages in 9.80 seconds (38968 msg/s, 1256 kB/s)

Receive 10 MB (= 10*1024*1024 bytes) and stop:

  $ s3t --role consumer --broker stomp://localhost:6163 \
    --destination /topic/test --count 10485760
  received 10 MB in 1.10 seconds (8870 kB/s)

=head1 SEE ALSO

L<Authen::Credential>.

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright CERN 2010-2012
