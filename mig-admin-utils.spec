%define real_version %((cat %{SOURCE1} || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)

Name:		mig-admin-utils
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Set of utilities for messaging broker administrators
License:	ASL 2.0
Group:		Applications/Internet
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	java-devel >= 0:1.5.0
BuildRequires:	jpackage-utils
Requires:	java >= 0:1.5.0
Requires:	jpackage-utils
Requires:	xerces-j2
BuildRequires:	xerces-j2, xml-commons-apis, ant, perl

%description
This package contains the following set of utilities for messaging
broker administrators:
amqmon   - ActiveMQ monitoring tool
amqrm    - remove ActiveMQ destinations or connections through JMX
aplomgmt - command line interface to Apollo's Management API
procstat - display per-process statistics
s3t      - STOMP Stress and Speed Tester
tcptop   - show the top usage of TCP connections
wypiwyc  - What You Produce Is What You Consume
xsv      - validate XML files against one or multiple XSDs

%prep
%setup -q -n %{name}-%{version}

%build
make build

%install
rm -rf %{buildroot}
make install INSTROOT=%{buildroot} INSTBINDIR=%{buildroot}%{_bindir} INSTMANDIR=%{buildroot}%{_mandir}

%clean
make clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES README LICENSE VERSION javadoc
/usr/share/%{name}
%{_bindir}/*
%{_mandir}/man?/*
